/*
*     Author:           Gabriel Mateu's
*     Github:           git.io/zemayer (really!)
*     Project:          Gitpress - Farmer Theme
*     Start In:         03/07/2012
*/

/* ==========>> Summary
    =Sintax Highlight Call
    =Variables
    =Mobile
        =MenuWidth
        =Menu-Button
        =Menu-Animate
        =Orientantion Change
    =Last-Child IE Hack
    =Resize Will be Deleted

============================================================= */

(function ($) {

    /* ===========>> [ =Variables ] ============  */
    var $rpButton     =   $('#hidden-menu-button'),
        $hiddenMenu   =   $('#menu-wrap'),
        $container    =   $('#container'),
        $content      =   $('.content'),
        $scrollToTop  =   $('#scroll-to-top'),
        $body         =   $('body'),
        $logo         =   $('#logo'),
        $mainMenu     =   $('.menu'),
        $search       =   $('.search'),
        $mainMenuLink =   $('.menu a'),
        $lastChild    =   $('.archive article');

    /* ===========>> [ =Sintax Highlight Call ] ============  */
    sh_highlightDocument();


    /*=====[ =Lazy Load ]=====*/
    //http://www.appelsiini.net/projects/lazyload
    $("img.lazy").lazyload({
        effect : "fadeIn"
    });

    /* ===========>> [ =Mobile ] ============  */
    if($body.width() <= 767 ){
        $('aside.menu-wrap').removeClass('menu-wrap').addClass('hidden-menu');
        $('.top').append($logo);
    }

    // /* ===========>> [ =Resize Browser - PLay OFF] ============  */
    $(window).resize(function() {
        if($body.width() <= 767 ){
            $('aside.menu-wrap').removeClass('menu-wrap').addClass('hidden-menu');
            $('.top').append($logo);
        }else{
            $('aside.hidden-menu').addClass('menu-wrap').removeClass('hidden-menu');
            $('.search').after($logo);
        }
    });

    // ===========>> =MenuWidth
    // Gets the width of the menu (relative of the view size)
    // Push the Menu of the screen
    var $hiddenMenuWidth  =  $hiddenMenu.width(),
        $hiddenMenuHeight =  $hiddenMenu.height(),
        $containerHeight  =  $container.height();

    $hiddenMenu.css('left', - $hiddenMenuWidth);

    var $position = $(window).scrollTop();
    console.log($position);

    // ===========>> =Menu-Button
    // Add class 'pressed' then run the animate functions for the menu
    $rpButton.on('click', function(e){
        e.preventDefault();
        e.stopPropagation();

        $(this).toggleClass('pressed');

        if(!$(this).hasClass('pressed')){
            HideMenu();
            $container.css('height', $containerHeight);
        }else{
            ShowMenu();
            $container.css('height', $hiddenMenuHeight);
            $(window).scrollTop($position);
        }
    });

    // ===========>> =Menu-Animate
    function ShowMenu() {
        $content.animate({
            left: $hiddenMenuWidth
        });

        $hiddenMenu.animate({
            left: 0
        });
    }

    function HideMenu() {
        $content.animate({
            left: 0
        });

        $hiddenMenu.animate({
            left: - $hiddenMenuWidth
        });
    }

    function disableScroll() {
        $(document).bind("touchmove",function(e){
            e.preventDefault();
        });
    }

    /* ===========>> [ =Orientantion Change - Mobile ] ============  */
    // $(window).addEventListener('orientationchange', handleOrientation, false);
    // function handleOrientation() {
    //     if (orientation == 0) {
    //       //portraitMode, do your stuff here
    //     }
    //     else if (orientation == 90) {
    //       //landscapeMode
    //     }
    //     else if (orientation == -90) {
    //       //landscapeMode
    //     }
    //     else if (orientation == 180) {
    //       //portraitMode
    //     }
    //     else {
    //     }
    // }

    /* ===========>> [ =Last-Child IE Hack ] ============  */
    if($.browser.msie){
        $lastChild.last().addClass('last-child');
    }

    /*=====[ =GetURL Active Page ]=====*/
    var url = window.location.pathname,
    // create regexp to match current url pathname and remove trailing slash if present as it could
    // collide with the link in navigation in case trailing slash wasn't present there
    urlRegExp = new RegExp(url.replace(/\/$/,'') + "$");
    // now grab every link from the navigation
    $('.menu a').each(function(){
        // and test its normalized href against the url pathname regexp
        if(urlRegExp.test(this.href.replace(/\/$/,''))){
            $(this).addClass('current');
        }
    });

})(jQuery);


